import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import db from '@/main'

var firebase = require("firebase/app");

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usuario: '',
    error: '',
    ventas: [],
    ventasBorradas: [],
    retiros: [],
    facturas: [],
    ediciones: [],
    copia: '',
    cajaDiaPrevio: {
      total: ''
    },
    cajaActual: '',
    copiaVenta: {
      cilindro: { kilo: '', cantidad: '', precioUnitario: '' },
      montoTotal: '',
      formaPago: '',
      comentario: '',
      despacho: '',
      hora: '',
      fecha: '',
      edited: '',
      id: ''
    },
    roles: [],
    rol: '',
    employees: [],
    employee: {
      nombre: '',
      apellido: '',
      rut: '',
      chofer: '',
      sueldos: [{ sueldo: '', kilosVendidos: '', descontado: '', bono: '', mes: '' }]
    },
    customers: [],
    discounts: [],
    texto: '',
    buscarcliente: '',
    carga: false,
    venta: {
      cilindro: { kilo: '', cantidad: '', precioUnitario: '' },
      montoTotal: '',
      formaPago: '',
      comentario: '',
      despacho: '',
      hora: '',
      fecha: '',
      edited: '',
      id: ''
    },
    hoy: '',
    factura: {
      cliente: { nombre: '', rut: '', ciudad: '' },
      total: '',
      cancelada: '',
      folio: '',
      fecha: '',
      fechaPago: ''
    },
    customer: {
      nombre: '',
      rut: '',
      ciudad: ''
    },
    admin: null,
    username: '',
    caja: 0,
    visas: 0,
    cajaAcum: 0,
    retiros: {
      fecha: '',
      monto: '',
      motivo: '',
      responsable: ''
    }
  },
  mutations: {
    setUsuario(state, payload) {
      state.usuario = payload
    },
    setError(state, payload) {
      state.error = payload
    },
    cargarFirebase(state, payload) {
      state.carga = payload
    },
    setVentas(state, ventas) {
      state.ventas = ventas
    },
    setVentasBorradas(state, ventas) {
      state.ventasBorradas = ventas
    },
    setRetiros(state, retiros) {
      state.retiros = retiros
    },
    setEdiciones(state, ediciones) {
      state.ediciones = ediciones
    },
    setVenta(state, venta) { // llena una venta del state con la recogida de la acción.
      state.venta = venta
    },
    setCopiaVenta(state, venta) { // llena una venta del state con la recogida de la acción.
      state.copiaVenta = venta
    },
    setRetiro(state, retiro) {
      state.retiro = retiro
    },
    setRoles(state, roles) {
      state.roles = roles
    },
    setRol(state, rol) {
      state.rol = rol
    },
    setEmployees(state, employees) {
      state.employees = employees
    },
    setEmployee(state, employee) {
      state.employee = employee
    },
    setCustomers(state, customers) {
      state.customers = customers
    },
    setDiscounts(state, discounts) {
      state.discounts = discounts
    },
    setToday(state, hoy) {
      state.hoy = hoy
    },
    setFactura(state, factura) {
      state.factura = factura
    },
    setCustomer(state, customer) {
      state.customer = customer
    },
    setFacturas(state, facturas) {
      state.facturas = facturas;
    },
    setCaja(state, caja) {
      state.caja = caja
    },
    setCajaDelDia(state, cajaDiaPrevio) {
      state.cajaDiaPrevio = cajaDiaPrevio
    },
    setCajaActual(state, cajaActual) {
      state.cajaActual = cajaActual;
    },
    setVisas(state, visas) {
      state.visas = visas
    },
    setCajaAcum(state, cajas) {
      state.cajaAcum = cajas
    },
    restarCaja(state, cash) {
      state.caja -= cash
    },
    borrarVenta(state, id) {
      state.ventas = state.ventas.filter(doc => {
        return doc.id != id
      })
    },
    deleteInvoice(state, id) {
      state.facturas = state.facturas.filter(doc => {
        return doc.id != id
      })
    },
    terminarVenta(state, id) {
      state.ventasBorradas = state.ventasBorradas.filter(doc => {
        return doc.id != id
      })
    },

    deleteCustomer(state, id) {
      state.customers = state.customers.filter(doc => {
        return doc.id != id
      })
    },

    deleteEmployee(state, id) {
      state.employees = state.employees.filter(doc => {
        return doc.id != id
      })
    }


  },
  actions: {
    buscador({ commit, state }, payload) {
      // console.log(payload);

      state.texto = payload.toLowerCase();

    },
    buscaClientes({ commit, state }, payload) {
      // console.log(payload);

      state.buscarcliente = payload.toLowerCase();

    },

    getVentasSomeDay({ commit }, date) {
      commit('cargarFirebase', true)

      const ventas = []
      var cash = 0;
      var visas = 0;

      db.collection(date).get().then(snapshot => {
        snapshot.forEach(doc => {
          let venta = doc.data();
          venta.id = doc.id;

          //  console.log(venta);
          ventas.push(venta);

          if (venta.formaPago == "Efectivo" && venta.montoTotal > 0) {
            cash += venta.montoTotal
          }
          if (venta.formaPago == "Visa") {
            visas += venta.montoTotal
          }

        })
        commit('setCaja', cash)
        commit('setVisas', visas)
        commit('cargarFirebase', false)
      })

      commit('setVentas', ventas)

    },


    getVentasToday({ commit }) {
      commit('cargarFirebase', true)
      const hora_fecha = new Date();

      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy
      // console.log(today)  
      const ventas = []
      var cash = 0;
      var visas = 0;

      db.collection(today).get().then(snapshot => {
        snapshot.forEach(doc => {
          let venta = doc.data();
          venta.id = doc.id;

          //  console.log(venta);
          ventas.push(venta);

          if (venta.formaPago == "Efectivo" && venta.montoTotal > 0) {
            cash += venta.montoTotal
          }
          if (venta.formaPago == "Visa") {
            visas += venta.montoTotal
          }

        })
        commit('cargarFirebase', false)
        commit('setCaja', cash)
        commit('setVisas', visas)
      })
      commit('setVentas', ventas)
      commit('setToday', today)
    },

    getEdiciones({ commit }, id_venta) {
      commit('cargarFirebase', true)
      const hora_fecha = new Date();

      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy
      // console.log(today)  
      var ediciones = []

      db.collection("ediciones-" + today).get().then(snapshot => {
        snapshot.forEach(doc => {
          let venta = doc.data();
          venta.id = doc.id;

          if (venta.id_referencia === id_venta) {
            ediciones.push(venta);
          }

        })
        commit('cargarFirebase', false)
      })

      commit('setEdiciones', ediciones)

    },

    getVentasBorradas({ commit }) {

      var collection = 'borradas';
      var borradas = [];
      commit('cargarFirebase', true)

      db.collection(collection).get().then(snapshot => {
        snapshot.forEach(doc => {
          let ventaBorrada = doc.data();
          ventaBorrada.id = doc.id;

          borradas.push(ventaBorrada);
        })
        commit('setVentasBorradas', borradas);
        commit('cargarFirebase', false);
      })

    },

    getVenta({ commit }, id) {
      const hora_fecha = new Date();
      commit('cargarFirebase', true)
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy;

      db.collection(today).doc(id).get()
        .then(doc => {
          let venta = doc.data();
          venta.id = doc.id;

          commit('setVenta', venta)
          commit('setCopiaVenta', venta)
          commit('cargarFirebase', false)

        })

    },

    getFactura({ commit }, id) {
      commit('cargarFirebase', true)
      let collection = 'facturas';

      db.collection(collection).doc(id).get()
        .then(doc => {
          let factura = doc.data();
          factura.id = doc.id;

          commit('setFactura', factura)
          commit('cargarFirebase', false)

        })

    },
    getCustomer({ commit }, id) {
      commit('cargarFirebase', true)
      let collection = 'clientes';

      db.collection(collection).doc(id).get()
        .then(doc => {
          let customer = doc.data();
          customer.id = doc.id;

          commit('setCustomer', customer)
          commit('cargarFirebase', false);

        })

    },

    agregarVenta({ commit }, respuestas) {
      commit('cargarFirebase', true)
      const user = firebase.auth().currentUser;

      const hora_fecha = new Date();

      var dd = String(hora_fecha.getDate()).padStart(2, '0');

      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy

      var hora = hora_fecha.getHours()
      var minutos = hora_fecha.getMinutes()
      if (hora < 10)
        hora = '0' + hora;
      if (minutos < 10)
        minutos = '0' + minutos;
      const hora_venta = hora + ":" + minutos

      db.collection(today).add({
        cilindro:
          { kilo: respuestas.kilo, cantidad: parseInt(respuestas.cantidad), precioUnitario: parseInt(respuestas.precioUnitario) },
        montoTotal: parseInt(respuestas.montoTotal),
        formaPago: respuestas.formaPago,
        hora: hora_venta,
        fecha: today,
        vendedor: { id: user.uid, email: user.email },
        despacho: respuestas.despacho,
        comentario: respuestas.comentario,
        edited: false
      })
        .then(doc => {
          // console.log(doc.id);
          router.push({ name: 'Inicio' })
          commit('cargarFirebase', false)
        })
    },

    cerrarCajaDelDia({ commit }, respuestas) {
      var collection = `cajas`

      db.collection(collection).doc(respuestas.fecha).set({
        total: respuestas.total
      })
        .then(function () {
          console.log("Se ha ingresado la caja del día");
        })
        .catch(function (error) {
          console.error("Error escribiendo el documento: ", error);
        });

    },


    getCajaDelDia({ commit }, id) {

      var collection = 'cajas'

      db.collection(collection).doc(id).get()
        .then(doc => {
          let caja = doc.data();
          caja.id = doc.id

          commit('setCajaDelDia', caja);

        })

    },
    getCajaActual({ commit }, id) {

      var collection = 'cajas'

      db.collection(collection).doc(id).get()
        .then(doc => {
          let caja = doc.data();
          caja.id = doc.id

          commit('setCajaActual', caja);

        })

    },

    agregarCliente({ commit }, respuestas) {
      commit('cargarFirebase', true)
      var collection = "clientes"

      db.collection(collection).add({
        nombre: respuestas.nombre,
        rut: respuestas.rut,
        ciudad: respuestas.ciudad,
      })
        .then(doc => {
          // console.log(doc.id);
          router.push({ name: 'ListaClientes' })
          commit('cargarFirebase', false)
        })
    },

    agregarTrabajador({ commit }, respuestas) {
      commit('cargarFirebase', true)
      const user = firebase.auth().currentUser;
      var collection = "trabajadores"

      db.collection(collection).add({
        nombre: respuestas.nombre,
        apellido: respuestas.apellido,
        rut: respuestas.rut,
        chofer: respuestas.chofer,
        fletero: false
      })
        .then(doc => {
          // console.log(doc.id);
          router.push({ name: 'Trabajadores' })
          commit('cargarFirebase', false)
        })
    },

    agregarFactura({ commit }, respuestas) {
      commit('cargarFirebase', true)
      var collection = "facturas"

      db.collection(collection).add({
        cliente: respuestas.cliente,
        total: parseInt(respuestas.total),
        fecha: respuestas.fecha,
        folio: parseInt(respuestas.folio),
        fechaPago: null,
        cancelada: false
      })
        .then(doc => {
          // console.log(doc.id);
          router.push({ name: 'Facturacion' })
          commit('cargarFirebase', false)
        })
    },

    agregarEdicionVenta({ commit }, payload) {
      commit('cargarFirebase', true)
      const user = firebase.auth().currentUser;
      const hora_fecha = new Date();
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy

      const hora = hora_fecha.getHours()
      var minutos = hora_fecha.getMinutes()
      if (minutos < 10)
        minutos = '0' + minutos;
      const hora_edicion = hora + ":" + minutos

      db.collection("ediciones-" + today).add({
        cilindro: { kilo: payload.kilo, cantidad: payload.cantidad, precioUnitario: payload.precioUnitario },
        montoTotal: payload.montoTotal,
        formaPago: payload.formaPago,
        vendedor: { id: user.uid, email: user.email },
        despacho: payload.despacho,
        comentario: payload.comentario,
        hora: payload.hora,
        fecha: payload.fecha,
        edicion: hora_edicion,
        id_referencia: payload.id
        // ],
        //cilindro: kilo


      })
        .then(doc => {
          // console.log(doc.id);
          commit('cargarFirebase', false)
        })
    },

    agregarVentaBorrada({ commit }, payload) {
      commit('cargarFirebase', true)
      const user = firebase.auth().currentUser;


      const hora_fecha = new Date();

      var dd = String(hora_fecha.getDate()).padStart(2, '0');

      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy;
      var hora = hora_fecha.getHours();
      var minutos = hora_fecha.getMinutes();
      if (minutos < 10)
        minutos = '0' + minutos;
      var hora_eliminacion = hora + ":" + minutos

      db.collection("borradas").add({
        cilindro: { kilo: payload.kilo, cantidad: payload.cantidad, precioUnitario: payload.precioUnitario },
        montoTotal: payload.montoTotal,
        formaPago: payload.formaPago,
        vendedor: { id: user.uid, email: user.email },
        despacho: payload.despacho,
        comentario: payload.comentario,
        hora: payload.hora,
        fecha: payload.fecha,
        hora_eliminacion: hora_eliminacion
        // ],
        //cilindro: kilo


      })
        .then(doc => {
          // console.log(doc.id);
          commit('cargarFirebase', false)
        })
    },

    eliminarVenta({ commit }, id) {
      const hora_fecha = new Date();
      commit('cargarFirebase', true)
      //const user = firebase.auth().currentUser;
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy

      var cash = 0;
      var visas = 0;

      db.collection(today).get().then(snapshot => {
        snapshot.forEach(doc => {
          let venta = doc.data();
          venta.id = doc.id;
          if (venta.formaPago == "Efectivo") {
            cash += venta.montoTotal;
          }
          if (venta.formaPago == "Visa") {
            visas += venta.montoTotal;
          }

        })
        commit('setCaja', cash);
        commit('setVisas', visas);

      })

      db.collection(today).doc(id).delete()
        .then(() => {
          console.log("Venta eliminada");
          commit('borrarVenta', id);
          commit('cargarFirebase', false)

        })
    },

    terminarVenta({ commit }, id) {
      commit('cargarFirebase', true);

      var collection = 'borradas';

      db.collection(collection).doc(id).delete()
        .then(() => {
          console.log("Venta eliminada para siempre");
          commit('terminarVenta', id);
          commit('cargarFirebase', false)

        })

    },

    editarVenta({ commit }, venta) {
      commit('cargarFirebase', true)
      const hora_fecha = new Date();
      //const user = firebase.auth().currentUser;
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy

      db.collection(today).doc(venta.id).update({
        cilindro: { kilo: venta.cilindro.kilo, cantidad: venta.cilindro.cantidad, precioUnitario: venta.cilindro.precioUnitario },
        montoTotal: venta.montoTotal,
        formaPago: venta.formaPago,
        comentario: venta.comentario,
        despacho: venta.despacho,
        edited: true
      })
        .then(() => {
          commit('cargarFirebase', false)
          router.push({ name: 'Inicio' })

        })
    },

    editarFactura({ commit }, factura) {
      commit('cargarFirebase', true)
      let collection = "facturas"
      db.collection(collection).doc(factura.id).update({
        cliente: factura.cliente,
        folio: parseInt(factura.folio),
        total: parseInt(factura.total),
        fecha: factura.fecha,
        cancelada: factura.cancelada,
        fechaPago: factura.fechaPago
      })
        .then(() => {
          commit('cargarFirebase', false)
          router.go(-1);

        })
    },
    editarCliente({ commit }, cliente) {
      commit('cargarFirebase', true)
      let collection = "clientes"
      db.collection(collection).doc(cliente.id).update({
        nombre: cliente.nombre,
        rut: cliente.rut,
        ciudad: cliente.ciudad
      })
        .then(() => {
          commit('cargarFirebase', false)
          router.push({ name: 'ListaClientes' })

        })
    },

    cajasAcum({ commit }) {
      commit('cargarFirebase', true)
      const hora_fecha = new Date();
      //const user = firebase.auth().currentUser;
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0');
      var yyyy = hora_fecha.getFullYear();
      const today = dd + "-" + mm + "-" + yyyy
      var cashAcum = 0
      var fechas = []

      for (var j = 9; j <= mm; j++) {

        for (var i = 1; i <= 31; i++) {
          if (i >= 10 && j < 10) {
            var dates = `${i}-0${j}-${yyyy}`
            fechas.push(dates);
          }
          else if (i < 10 && j < 10) {
            var dates = `0${i}-0${j}-${yyyy}`
            fechas.push(dates);
          }

          else if (i < 10 && j >= 10) {
            var dates = `0${i}-${j}-${yyyy}`
            fechas.push(dates);
          }

          else {
            var dates = `${i}-${j}-${yyyy}`
            fechas.push(dates);
          }
        }
      }
      for (var k = 0; k < fechas.length; k++) {
        db.collection(fechas[k]).get().then(snapshot => {
          snapshot.forEach(doc => {
            let venta = doc.data();
            venta.id = doc.id;

            if (venta.formaPago == "Efectivo" && venta.montoTotal > 0) {
              cashAcum += venta.montoTotal;
            }
          })

          commit('setCajaAcum', cashAcum)
          commit('cargarFirebase', false)
        })
      }
    },

    getRetiros({ commit }) {
      commit('cargarFirebase', true)
      const retiros = []
      var coleccion = `retiros`

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let retiro = doc.data();
          // console.log(doc.data);
          retiro.id = doc.id;

          retiros.push(retiro);
        })
        commit('cargarFirebase', false)

      })
      commit('setRetiros', retiros)

    },

    getFacturas({ commit }) {
      commit('cargarFirebase', true)
      var facturas = []
      var coleccion = `facturas`

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let factura = doc.data();
          // console.log(doc.data);
          factura.id = doc.id;
          facturas.push(factura);
        })
        // commit('setMontoRetiradoAcum', cashTotal)
        commit('cargarFirebase', false)

      })
      commit('setFacturas', facturas)

    },

    generarRetiro({ commit }, respuestas) {
      commit('cargarFirebase', true)
      const user = firebase.auth().currentUser;
      const hora_fecha = new Date();
      var dd = String(hora_fecha.getDate()).padStart(2, '0');
      var mm = String(hora_fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = hora_fecha.getFullYear();
      const today = `${yyyy}-${mm}-${dd}`
      var signo = -1
      const coleccion = `retiros`

      var hora = hora_fecha.getHours();
      var minutos = hora_fecha.getMinutes();

      if (hora < 10)
        hora = '0' + hora;
      if (minutos < 10)
        minutos = '0' + minutos;
      const hora_retiro = hora + ":" + minutos

      if (!respuestas.retirar) {
        signo *= -1
      }

      db.collection(coleccion).add({
        monto: parseInt(respuestas.monto * signo),
        motivo: respuestas.motivo,
        responsable: user.email,
        fecha: today,
        hora: hora_retiro,
        retirar: respuestas.retirar
      })
        .then(doc => {
          //console.log(doc.id);
          commit('cargarFirebase', false)
          router.push({ name: 'Inicio' })
        })
    },

    crearUsuario({ commit }, payload) {
      firebase.auth().createUserWithEmailAndPassword(payload.email.trim(), payload.pass)
        .then(res => {
          console.log(res.user.uid);
          console.log(res.user.email);
          commit('setUsuario', { email: res.user.email, uid: res.user.uid });

          // Crear una colección
          db.collection(res.user.email).add({
            nombre: 'tarea de ejemplo'
          })
            .then(() => {
              router.push({ name: 'Inicio' });
            })

        })
        .catch(err => {
          console.log(err.message);
          commit('setError', err.message);
        })
    },

    isAdmin({ commit }, payload) {
      for (var i = 0; i < payload.roles.length; i++) {
        if (payload.roles[i].idUser == payload.usuario) {
          var username = payload.roles[i].nombre;
          if (payload.roles[i].admin) {
            username = payload.roles[i].nombre;
            commit('setUsername', username)
            commit('setAdmin', true)
            break;
          }
          else {
            commit('setUsername', username)
            commit('setAdmin', false);
          }
        }

      }
    },

    ingresoUsuario({ commit }, payload) {
      commit('cargarFirebase', true)
      var coleccion = `user-roles`;
      var roles = [];
      firebase.auth().signInWithEmailAndPassword(payload.email.trim(), payload.pass)
        .then(res => {
          console.log(res);
          commit('setUsuario', { email: res.user.email, uid: res.user.uid });

          router.push({ name: 'Inicio' });
        })
        .catch(err => {
          console.log(err.message);
          commit('cargarFirebase', false);
          commit('setError', err.message);
        })

    },

    getRoles({ commit }) {
      commit('cargarFirebase', true)
      var coleccion = `user-roles`;
      var roles = [];

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let rol = doc.data();
          rol.id = doc.id;
          roles.push(rol);

        })
        commit('setRoles', roles)
        commit('cargarFirebase', false);

      })

    },
    getRol({ commit }, id) {
      var collection = "roles"
      commit('cargarFirebase', true)

      db.collection(collection).doc(id).get()
        .then(doc => {
          let rol = doc.data();
          rol.id = doc.id;
          commit('setRol', rol)

        })

      commit('cargarFirebase', false)

    },

    getEmployees({ commit }) {
      commit('cargarFirebase', true)
      var coleccion = `trabajadores`;
      var employees = []

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let employee = doc.data();
          employee.id = doc.id;
          employees.push(employee);

        })
        commit('setEmployees', employees)
        commit('cargarFirebase', false);

      })
    },

    getEmployee({ commit }, id) {
      var collection = "trabajadores"
      commit('cargarFirebase', true)

      db.collection(collection).doc(id).get()
        .then(doc => {
          // console.log(doc.data());
          // console.log(doc.id)
          let employee = doc.data();
          employee.id = doc.id;
          commit('setEmployee', employee)

        })

      commit('cargarFirebase', false)

    },

    getCustomers({ commit }) {
      commit('cargarFirebase', true)
      var coleccion = `clientes`;
      var customers = []

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let customer = doc.data();
          customer.id = doc.id;
          customers.push(customer);

        })
        commit('setCustomers', customers)
        commit('cargarFirebase', false);

      })
    },

    getDiscounts({ commit }) {
      commit('cargarFirebase', true)
      var coleccion = `descuentos`;
      var discounts = []

      db.collection(coleccion).get().then(snapshot => {
        snapshot.forEach(doc => {
          let discount = doc.data();
          discount.id = doc.id;
          discounts.push(discount);

        })
        commit('setDiscounts', discounts)
        commit('cargarFirebase', false);

      })
    },

    deleteCustomer({ commit }, id) {
      commit('cargarFirebase', true);

      var collection = 'clientes';

      db.collection(collection).doc(id).delete()
        .then(() => {
          console.log("Cliente eliminado");
          commit('deleteCustomer', id);
          commit('cargarFirebase', false)

        })

    },


    deleteEmployee({ commit }, id) {
      commit('cargarFirebase', true);

      var collection = 'trabajadores';

      db.collection(collection).doc(id).delete()
        .then(() => {
          console.log("Trabajador eliminado");
          commit('deleteEmployee', id);
          commit('cargarFirebase', false)

        })

    },

    deleteInvoice({ commit }, id) {
      commit('cargarFirebase', true);
      var collection = 'facturas';

      db.collection(collection).doc(id).delete()
        .then(() => {
          console.log("Factura eliminada");
          commit('deleteInvoice', id);
          commit('cargarFirebase', false)

        })

    },

    detectarUsuario({ commit }, payload) {
      if (payload != null) {
        commit('setUsuario', { email: payload.email, uid: payload.uid })
      }
      else {
        commit('setUsuario', null)
      }

    },

    cerrarSesion({ commit }) {
      firebase.auth().signOut()
      commit('setUsuario', null);
      commit('setRol', '');
      router.push({ name: 'Ingreso' })

    }

  },
  getters: {
    existeUsuario(state) {
      if (state.usuario === null || state.usuario === '' || state.usuario === undefined) {
        return false
      } else {
        return true
      }
    },

    retirosFiltrados(state) {
      let arregloFiltrado = []
      for (let retiro of state.retiros) {
        let motivo = retiro.motivo.toLowerCase();
        let responsable = retiro.responsable.toLowerCase();
        let fecha = retiro.fecha.toLowerCase();
        let hora = retiro.hora.toLowerCase();

        if (motivo.indexOf(state.texto) >= 0 || responsable.indexOf(state.texto) >= 0 || hora.indexOf(state.texto) >= 0 || fecha.indexOf(state.texto) >= 0)
          arregloFiltrado.push(retiro)
      }
      return arregloFiltrado;
    },

    clientesFiltrados(state) {
      let arregloFiltrado = []
      for (let cliente of state.customers) {
        let nombre = cliente.nombre.toLowerCase();
        let rut = cliente.rut.toLowerCase();
        let ciudad = cliente.ciudad.toLowerCase();

        if (nombre.indexOf(state.buscarcliente) >= 0 || rut.indexOf(state.buscarcliente) >= 0 || ciudad.indexOf(state.buscarcliente) >= 0)
          arregloFiltrado.push(cliente)
      }
      return arregloFiltrado;
    },

    arrayOrdenado(state) {
      let arregloOrdenado = []
      arregloOrdenado = state.ventas.sort(function (a, b) {
        var x = a.hora;
        var y = b.hora;

        if (x < y) return -1
        if (x > y) return 1
      });
      return arregloOrdenado;
    },

    edicionesOrdenadas(state) {
      let arregloOrdenado = []
      arregloOrdenado = state.ediciones.sort(function (a, b) {
        var x = a.edicion;
        var y = b.edicion;

        if (x < y) return -1
        if (x > y) return 1
      });
      return arregloOrdenado;
    },

    sortedCustomers(state) {
      let sortedCustomers = state.customers.sort((a, b) => (a.nombre > b.nombre) ? 1 : (b.nombre > a.nombre) ? -1 : 0)
      return sortedCustomers;
    },

  },
  modules: {
  }
})
