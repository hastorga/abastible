import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuelidate from 'vuelidate'
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './assets/sass/light-bootstrap-dashboard.scss'


// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(Vuelidate)

var firebase = require("firebase/app");

require("firebase/auth");
// require("firebase/database");
require("firebase/firestore");
require("firebase/storage");
// require("firebase/messaging");
// require("firebase/functions");

var firebaseConfig = {
  apiKey: "AIzaSyCnQq2ngke71d69QE_CuxvdHOfFYlheaas",
  authDomain: "abastible-473.firebaseapp.com",
  databaseURL: "https://abastible-473.firebaseio.com",
  projectId: "abastible-473",
  storageBucket: "abastible-473.appspot.com",
  messagingSenderId: "990556519412",
  appId: "1:990556519412:web:c83e2cbc48c952ebf617a1",
  measurementId: "G-K87S3XHE59"
};
// Initialize Firebase
const fireApp = firebase.initializeApp(firebaseConfig);

const db = fireApp.firestore();
const storage = firebase.storage();

fireApp.firestore().settings({
  //Settings properties
  //cacheSizeBytes: 0,
  //host: '',
  //ssl: true
})

export default db

firebase.auth().onAuthStateChanged((user) => {
  //console.log(user)
  if (user) {
    store.dispatch('detectarUsuario', { email: user.email, uid: user.uid })
  }
  else {
    store.dispatch('detectarUsuario', null)

  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
