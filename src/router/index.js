import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)
var firebase = require("firebase/app");

const routes = [

  {
    path: '/',
    name: 'Inicio',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Inicio.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/ingreso',
    name: 'Ingreso',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Ingreso.vue')
  },

  {
    path: '/editar/:id',
    name: 'Editar',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Editar.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/agregar',
    name: 'Agregar',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AgregarVenta.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/resumen',
    name: 'Sumario',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Sumario.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/retirar',
    name: 'RetiroCaja',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/RetirarCaja.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/deletedSells/',
    name: 'VentasBorradas',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/VentasBorradas.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/trabajadores/',
    name: 'Trabajadores',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Trabajadores.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/registrocliente/',
    name: 'AgregarCliente',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AgregarCliente.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/descuentos-realizados/',
    name: 'ListaDescuentos',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ListaDescuentos.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/clientes-inscritos/',
    name: 'ListaClientes',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ListaClientes.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/registro-trabajador/',
    name: 'AgregarTrabajador',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AgregarTrabajador.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/trabajador/:id',
    name: 'Trabajador',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Trabajador.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/facturacion',
    name: 'Facturacion',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Facturacion.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/ingresoFactura',
    name: 'IngresarFactura',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/IngresarFactura.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/editar-factura/:id',
    name: 'EditarFactura',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/EditarFactura.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/editar-cliente/:id',
    name: 'EditarCliente',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/EditarCliente.vue'),
    meta: { requiresAuth: true }
  },

  {
    path: '/facturas-cliente/:id',
    name: 'FacturasPorCliente',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/FacturasPorCliente.vue'),
    meta: { requiresAuth: true }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  duplicateNavigationPolicy: 'ignore'

})


router.beforeEach((to, from, next) => {
  const rutaProtegida = to.matched.some(record => record.meta.requiresAuth);
  const user = firebase.auth().currentUser;

  if (rutaProtegida === true && user === null) {
    next({ name: 'Ingreso' })
  }
  else {
    next()
  }
})

export default router
